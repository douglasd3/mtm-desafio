//
//  MaskRow.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 14/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Foundation
import Eureka
import InputMask

public class MaskCell: _FieldCell<String>, CellType, MaskedTextFieldDelegateListener {
    
    var maskedDelegate: MaskedTextFieldDelegate!
    var textMask: String?
    var rawValue: String?
    
    public override func setup() {
        super.setup()
    }
    
    public override func update() {
        super.update()
        
        if let textMask = textMask {
            maskedDelegate = MaskedTextFieldDelegate(format: textMask)
            maskedDelegate.listener = self
            
            textField.delegate = maskedDelegate
        }
        
    }
    
    open func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String
        ) {
        
        row.value = textField.text
        rawValue = value        
    }
}

public class MaskRow<Cell: MaskCell>: FieldRow<Cell> {
    var rawValue: String? {
        get {
            return cell.rawValue
        }
    }
    
    var textMask: String? {
        get {
            return cell.textMask
        }
        
        set(newValue) {
            cell.textMask = newValue
        }
    }
    
    required public init(tag: String?) {
        super.init(tag: tag)
    }
}

////////////////////////////////////////////

public class PhoneMaskCell: MaskCell {
    open override func setup() {
        super.setup()
        textField.keyboardType = .phonePad
    }
}

public final class PhoneMaskRow: MaskRow<PhoneMaskCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        
        textMask = "([00]) [00000]-[0000]"
        let ruleRequiredViaClosure = RuleClosure<String> { rowValue in
            if let value = rowValue {
                return (!value.isValidPhoneNumber) ? ValidationError(msg: R.string.localizable.invalidPhone()) : nil
            }
            return nil
        }
        add(rule: ruleRequiredViaClosure)
        validationOptions = .validatesOnBlur
    }
}

public class CpfMaskCell: MaskCell {
    open override func setup() {
        super.setup()
        textField.keyboardType = .numberPad
    }
}

public final class CpfMaskRow: MaskRow<CpfMaskCell>, RowType {
    required public init(tag: String?) {
        super.init(tag: tag)
        
        textMask = "[000].[000].[000]-[00]"
        let ruleRequiredViaClosure = RuleClosure<String> { rowValue in
            if let value = rowValue {
                return (!value.isValidCPF) ? ValidationError(msg: R.string.localizable.invalidCpf()) : nil
            }
            return nil
        }
        add(rule: ruleRequiredViaClosure)
    }
}

