//
//  CategoryDetailCell.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 21/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import UIKit

class CategoryDetailCell: UITableViewCell {

    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var jobLabel: UILabel!
    
    var viewModel: CategoryDetailItemVM! {
        didSet {
            nameLabel.text = viewModel.employeeName
            jobLabel.text = viewModel.employeeJob
        }
    }
    
}
