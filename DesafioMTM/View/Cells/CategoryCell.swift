//
//  CategoryCell.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 19/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var categoryIconImageView: UIImageView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    
    var viewModel: CategoryItemVM! {
        didSet {
            categoryNameLabel.text = viewModel.categoryName
            categoryIconImageView.image = UIImage(resource: viewModel.categoryImageResource!)
        }
    }
    
}
