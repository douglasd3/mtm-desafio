//
//  NewAccountVC.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 12/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import UIKit
import Eureka
import InputMask

class NewAccountVC: FormViewController, ValidateFormProtocol {
    
    // MARK: - ViewModel
    
    var viewModel: NewAccountViewModel!
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()                
        setup()
    }
    
}

// MARK: Setup UI

extension NewAccountVC {
    
    fileprivate func setup() {
        setupForm()
    }
    
    fileprivate func setupForm() {
        form +++ Section()
            <<< EmailRow("email"){ row in
                row.title = R.string.localizable.email()
                let ruleRequiredViaClosure = RuleClosure<String> { rowValue in
                    return (rowValue == nil || rowValue!.isEmpty) ? ValidationError(msg: R.string.localizable.invalidEmailEmpty()) : nil
                }
                row.add(rule: ruleRequiredViaClosure)
                
                let ruleEmailRequiredViaClosure = RuleClosure<String> { rowValue in
                    return (rowValue == nil || !rowValue!.isValidEmail) ? ValidationError(msg: R.string.localizable.invalidEmail()) : nil
                }
                row.add(rule: ruleEmailRequiredViaClosure)
                
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
            }
            
            <<< PasswordRow("password"){ row in
                row.title = R.string.localizable.password()
                let ruleRequiredViaClosure = RuleClosure<String> { rowValue in
                    return (rowValue == nil || rowValue!.isEmpty) ? ValidationError(msg: R.string.localizable.invalidPasswordEmpty()) : nil
                }
                row.add(rule: ruleRequiredViaClosure)
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
            }
            
            <<< PhoneMaskRow("phone"){ row in
                row.title = R.string.localizable.phone()
                row.placeholder = R.string.localizable.newAccountPhonePlaceholder()
                let ruleRequiredViaClosure = RuleClosure<String> { rowValue in
                    return (rowValue == nil || rowValue!.isEmpty) ? ValidationError(msg: R.string.localizable.invalidPhoneEmpty()) : nil
                }
                row.add(rule: ruleRequiredViaClosure)
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
                    
            }
            <<< CpfMaskRow("cpf"){ row in
                row.title = R.string.localizable.cpf()
                let ruleRequiredViaClosure = RuleClosure<String> { rowValue in
                    return (rowValue == nil || rowValue!.isEmpty) ? ValidationError(msg: R.string.localizable.invalidCpfEmpty()) : nil
                }
                row.add(rule: ruleRequiredViaClosure)
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
                    
            }
            
            +++ Section()
            <<< ButtonRow() { row in
                row.title = R.string.localizable.send()
                }
                .onCellSelection() { cell, row in
                    self.sendNewAccount()
                    
        }
    }
    
}

// MARK: - Helpers

extension NewAccountVC {
    
    fileprivate func sendNewAccount() {
        if formIsValid() {        
            var email = ""
            var password = ""
            var phone = ""
            var cpf = ""
            
            for rowValue in form.values() {
                switch rowValue.key {
                case "email":
                    email = rowValue.value as! String
                case "password":
                    password = rowValue.value as! String
                case "phone":
                    phone = rowValue.value as! String
                case "cpf":
                    cpf = rowValue.value as! String
                default:
                    break
                }
            }
            
            viewModel.sendNewAccount(withEmail: email, password, phone, andCpf: cpf)
        }        
    }
    
}

// MARK: ViewModelDelegate

extension NewAccountVC: NewAccountViewModelDelegate {
    
    func apiCallDidFinish(error: Error?, message: String?) {
        if error != nil && message != nil {
            MessagesUtil.showAlert(withTitle: R.string.localizable.failureMessageTitle(), message: message!, andType: .failure)
        }
    }
    
}
