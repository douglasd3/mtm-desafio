//
//  CategoriesVC.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 10/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import UIKit
import StatefulViewController

class CategoriesVC: UIViewController, LoadingStatePresentableViewController {
    
    // MARK: IBOutlet
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - ViewModel
    
    var viewModel: CategoriesViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        
        loadCategories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

}

// MARK: StatefulViewController

extension CategoriesVC: StatefulViewController {
    
    func hasContent() -> Bool {
        return viewModel.hasContent
    }
    
}

// MARK: Setup UI

extension CategoriesVC {
    
    fileprivate func setup() {
        setupTableView()
        setupNavigationBar()
        setupLoadingState()
    }
    
    fileprivate func setupNavigationBar() {
        title = R.string.localizable.categoriesTitle()
        navigationController?.navigationBar.applyBaseStyle()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    fileprivate func setupTableView() {
        tableView.register(R.nib.categoryCell)        
    }
    
}

// MARK: Helpers

extension CategoriesVC {
    
    fileprivate func loadCategories() {
        startLoading()
        viewModel.fetchCategories()
    }
    
}

// MARK: TableViewDelegate and DataSource

extension CategoriesVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CategoryCell.self)) as! CategoryCell
        
        cell.accessoryType = .disclosureIndicator
        
        cell.viewModel = CategoryItemVM(model: viewModel.dataSource[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath) as! CategoryCell
        
        viewModel.itemSelected(item: cell.viewModel.createDetailViewModel())
    }
    
}

// MARK: ViewModelDelegate

extension CategoriesVC: CategoriesViewModelDelegate {
    
    func apiCallDidFinish(error: Error?, message: String?) {
        if error != nil && message != nil {
            MessagesUtil.showAlert(withTitle: R.string.localizable.failureMessageTitle(), message: message!, andType: .failure)
            endLoading(error: error)
        }
        else {
            tableView.reloadData()
            endLoading()
        }
    }
    
}
