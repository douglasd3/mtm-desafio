//
//  AuthenticationVC.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 10/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import UIKit
import Eureka
import SwiftyColor
import GoogleSignIn
import SnapKit

class AuthenticationVC: FormViewController, ValidateFormProtocol, GIDSignInUIDelegate {
    
    // MARK: - ViewModel
    
    var viewModel: AuthenticationVM!
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

}

// MARK: Setup UI

extension AuthenticationVC {
    
    fileprivate func setup() {
        setupInitialValues()
        setupForm()
    }
    
    fileprivate func setupInitialValues() {
        title = R.string.localizable.authTitle()
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    fileprivate func setupForm() {
        form +++ Section(R.string.localizable.authHeader()){ section in
            
            section.header = {
                var header = HeaderFooterView<UIView>(.callback({
                    let view = UIView()
                    
                    let googleButton = GIDSignInButton()
                    
                    view.addSubview(googleButton)
                    
                    googleButton.snp.makeConstraints { make in
                        make.left.equalTo(view.snp.left).offset(14)
                        make.right.equalTo(view.snp.right).offset(-14)
                    }
                    
                    
                    return view
                }))
                header.height = { 50 }
                return header
            }()
            
            }
            <<< EmailRow("email"){ row in
                row.title = R.string.localizable.email()
                let ruleRequiredViaClosure = RuleClosure<String> { rowValue in
                    return (rowValue == nil || rowValue!.isEmpty) ? ValidationError(msg: R.string.localizable.invalidEmailEmpty()) : nil
                }
                row.add(rule: ruleRequiredViaClosure)
                
                let ruleEmailRequiredViaClosure = RuleClosure<String> { rowValue in
                    return (rowValue == nil || !rowValue!.isValidEmail) ? ValidationError(msg: R.string.localizable.invalidEmail()) : nil
                }
                row.add(rule: ruleEmailRequiredViaClosure)
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
            }
            
            <<< PasswordRow("password"){ row in
                row.title = R.string.localizable.password()
                let ruleRequiredViaClosure = RuleClosure<String> { rowValue in
                    return (rowValue == nil || rowValue!.isEmpty) ? ValidationError(msg: R.string.localizable.invalidEmailEmpty()) : nil
                }
                row.add(rule: ruleRequiredViaClosure)
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
            }
            
            +++ Section()
            <<< ButtonRow() { row in
                row.title = R.string.localizable.enter()
                }.onCellSelection() { cell, row in
                    self.loginAction()
                    
            }
            
            +++ Section()
            <<< ButtonRow() { row in
                row.title = R.string.localizable.createAccount()
                }
                .onCellSelection() { cell, row in
                    self.showNewAccountVC()
        }
    }
    
}

// MARK: - Helpers

extension AuthenticationVC {
    
    fileprivate func showNewAccountVC() {
        viewModel.showNewAccountVC()
    }
    
    fileprivate func loginAction() {
        if formIsValid() {
            var email = ""
            var password = ""
            
            for rowValue in form.values() {
                switch rowValue.key {
                case "email":
                    email = rowValue.value as! String
                case "password":
                    password = rowValue.value as! String
            
                default:
                    break
                }
            }
            
            viewModel.signIn(withEmail: email, andPassword: password)
        }
    }
    
}

// MARK: ViewModelDelegate

extension AuthenticationVC: AuthenticationViewModelDelegate {
    
    func apiCallDidFinish(error: Error?, message: String?) {
        if error != nil && message != nil {
            MessagesUtil.showAlert(withTitle: R.string.localizable.failureMessageTitle(), message: message!, andType: .failure)
        }
    }
    
}
