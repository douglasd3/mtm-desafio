//
//  EmployeeDetailVC.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 21/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import UIKit
import MessageUI

class EmployeeDetailVC: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var jobLabel: UILabel!
    @IBOutlet weak var birthdayButton: UILabel!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // MARK: - ViewModel
    
    var viewModel: EmployeeDetailViewModel!
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        
        loadEmployeeInfo()
    }
    
}

// MARK: Actions

extension EmployeeDetailVC {
    
    @IBAction func emailAction(_ sender: Any) {
        viewModel.showEmailComposer()
    }
    
    @IBAction func phoneAction(_ sender: Any) {
        viewModel.showPhoneApp()
    }
    
}

// MARK: Setup UI

extension EmployeeDetailVC {
    
    fileprivate func setup() {
        setupNavigationBar()
    }
    
    fileprivate func setupNavigationBar() {
        title = viewModel.employeeDetailTitle
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
}

// MARK: Helpers

extension EmployeeDetailVC {
    
    fileprivate func loadEmployeeInfo () {
        nameLabel.text = viewModel.employeeName
        jobLabel.text = viewModel.employeeJob
        birthdayButton.text = "Data de nascimento: \(viewModel.employeeBirthday)"
        emailButton.setTitle(viewModel.employeeEmail, for: .normal)
        phoneButton.setTitle(viewModel.employeePhone, for: .normal)
        descriptionLabel.text = viewModel.employeeDescription
    }
    
}

// MARK: MFMailComposeViewControllerDelegate

extension EmployeeDetailVC: MFMailComposeViewControllerDelegate  {    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
