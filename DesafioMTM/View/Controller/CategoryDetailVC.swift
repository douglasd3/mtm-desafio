//
//  CategoryDetailVC.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 21/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import UIKit
import StatefulViewController

class CategoryDetailVC: UIViewController, LoadingStatePresentableViewController {
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - ViewModel
    
    var viewModel: CategoryDetailViewModel!
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        
        loadEmployees()
    }

}

// MARK: StatefulViewController

extension CategoryDetailVC: StatefulViewController {
    
    func hasContent() -> Bool {
        return viewModel.hasContent
    }
    
}


// MARK: Setup UI

extension CategoryDetailVC {
    
    fileprivate func setup() {
        setupTableView()
        setupNavigationBar()
        setupLoadingState()
    }
    
    fileprivate func setupNavigationBar() {
        title = viewModel.categoryName
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    fileprivate func setupTableView() {
        tableView.register(R.nib.categoryDetailCell)        
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
}

// MARK: Helpers

extension CategoryDetailVC {
    
    fileprivate func loadEmployees() {
        startLoading()
        viewModel.fetchEmployees()
    }
    
}
// MARK: TableViewDelegate and DataSource

extension CategoryDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CategoryDetailCell.self)) as! CategoryDetailCell
        cell.accessoryType = .disclosureIndicator
        
        cell.viewModel = CategoryDetailItemVM(model: viewModel.dataSource[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell = tableView.cellForRow(at: indexPath) as! CategoryDetailCell
        
        viewModel.itemSelected(item: cell.viewModel.createDetailViewModel())
    }
    
}


// MARK: ViewModelDelegate

extension CategoryDetailVC: CategoryDetailViewModelDelegate {
    
    func apiCallDidFinish(error: Error?, message: String?) {
        if error != nil && message != nil {
            MessagesUtil.showAlert(withTitle: R.string.localizable.failureMessageTitle(), message: message!, andType: .failure)
            endLoading(error: error)
        }
        else {
            tableView.reloadData()
            endLoading()
        }
    }
    
}


