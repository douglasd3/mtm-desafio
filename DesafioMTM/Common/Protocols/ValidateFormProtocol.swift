//
//  ValidateFormProtocol.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 19/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Eureka

protocol ValidateFormProtocol {
    
    func formIsValid() -> Bool
    
}

extension ValidateFormProtocol where Self: FormViewController {
    
    func formIsValid() -> Bool {
        for validationError in form.validate() {
            MessagesUtil.showAlert(withTitle: R.string.localizable.failureMessageTitle(), message: validationError.msg, andType: .failure)
            return false
        }
        
        return true
    }
    
}
