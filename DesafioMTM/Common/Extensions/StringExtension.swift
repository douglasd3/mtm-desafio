//
//  StringExtension.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 12/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Foundation
import PhoneNumberKit

extension String {
    
    var trimmed: String {
        get {
            return trimmingCharacters(in: NSCharacterSet.whitespaces)
        }
    }
    
    var onlyNumbers: String {
        get {
            return replacingOccurrences(of: "[^0-9]", with: "", options: String.CompareOptions.regularExpression, range: nil)
        }
    }
    
    var isValidEmail: Bool {
        get {
            let pattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            let predicate = NSPredicate(format:"SELF MATCHES %@", pattern)
            return predicate.evaluate(with: trimmed.lowercased())
        }
    }
    
    var isValidPhoneNumber: Bool {
        get {
            let phoneNumberKit = PhoneNumberKit()
            
            let number = onlyNumbers
            
            do {
                _ = try phoneNumberKit.parse("+55\(number)")
                return true
            }
            catch {
                return false
            }
        }
    }
    
    var isValidCPF: Bool {
        get {
            let cpf = onlyNumbers
            
            if cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999" || cpf.characters.count != 11 {
                return false
            }
            
            var soma = 0;
            var peso: Int
            
            let digito_verificador_10 = Int(String(Array(cpf.characters)[9]))
            let digito_verificador_11 = Int(String(Array(cpf.characters)[10]))
            
            var digito_verificador_10_correto: Int
            var digito_verificador_11_correto: Int
            
            // Verificação 10 Digito
            peso = 10
            for i in 0 ..< 9 {
                soma = soma + (Int(String(Array(cpf.characters)[i]))! * peso)
                peso = peso - 1;
            }
            
            if (soma % 11 < 2) {
                digito_verificador_10_correto = 0
            }else{
                digito_verificador_10_correto = 11 - (soma % 11)
            }
            
            // Verifição 11 Digito
            soma = 0
            peso = 11
            for i in 0 ..< 10 {
                soma = soma + (Int(String(Array(cpf.characters)[i]))! * peso)
                peso = peso - 1
            }
            
            if (soma % 11 < 2) {
                digito_verificador_11_correto = 0
            }
            else {
                digito_verificador_11_correto = 11 - (soma % 11)
            }
            
            if (digito_verificador_10_correto == digito_verificador_10 && digito_verificador_11_correto == digito_verificador_11) {
                return true
            }
            else {
                return false
            }   
        }
    }
    
}
