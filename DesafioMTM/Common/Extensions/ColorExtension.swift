//
//  ColorExtension.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 12/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import SwiftyColor

public extension Color {
    
    public class var mainColor: Color {
        return 0xffffff.color
    }
    
    public class var accentColor: Color {
        return 0x3db790.color
    }
    
    public class var backgroundColor: Color {
        return 0xececec.color
    }
    
}

