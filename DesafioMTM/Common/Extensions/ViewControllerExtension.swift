//
//  ViewControllerExtension.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 12/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import UIKit
import SnapKit
import SwiftyColor
import SpringIndicator

fileprivate var indicatorContainer = UIView()

extension UIViewController {    
    
    func setupModal(withTitle title: String = "") {
        let backButton = UIBarButtonItem(title: "Voltar", style: .plain, target: self, action: #selector(self.dismissModal))
        
        navigationItem.leftBarButtonItem = backButton
        navigationItem.title = title
    }
    
    func dismissModal() {
        dismiss(animated: true, completion: nil)
    }
    
    fileprivate func createIndicatorContainer()-> UIView {
        let indicatorContainer = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        let indicator = SpringIndicator(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        
        indicator.lineWidth = 2
        indicator.lineColor = Color.mainColor
        indicator.startAnimation()
        indicatorContainer.addSubview(indicator)
        
        indicatorContainer.snp.makeConstraints { make in
            make.size.equalTo(indicator)
        }
        
        return indicatorContainer
    }
    
    func showIndicator() {
        view.endEditing(true)
        let indicator = createIndicatorContainer()
        indicatorContainer = UIView()
        
        indicatorContainer.backgroundColor = Color(red: 0, green: 0, blue: 0, alpha: 0.5)
        indicatorContainer.addSubview(indicator)
        view.addSubview(indicatorContainer)
        
        indicatorContainer.snp.makeConstraints { make in
            make.edges.equalTo(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
        }
        indicator.snp.makeConstraints { make in
            make.center.equalTo(indicatorContainer)
        }
    }
    
    func dismissIndicator() {
        indicatorContainer.removeFromSuperview()
    }
    
}
