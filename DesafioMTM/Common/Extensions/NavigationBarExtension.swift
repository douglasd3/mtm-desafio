//
//  NavigationBarExtension.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 19/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Foundation
import SwiftyColor

extension UINavigationBar {
    
    func applyBaseStyle() {
        barTintColor        = Color.mainColor
        tintColor           = Color.black
        isTranslucent         = false
        titleTextAttributes = [NSForegroundColorAttributeName:Color.black]
    }
    
}

