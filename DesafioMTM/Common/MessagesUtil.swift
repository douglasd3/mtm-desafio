//
//  MessagesUtil.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 14/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import SwiftMessages

struct MessagesUtil {
    enum MessageType {
        case success
        case failure
        case warning
        case info
    }
    
    static func showAlert(withTitle title: String, message: String, andType type: MessageType) {
        let view = MessageView.viewFromNib(layout: .MessageView)
        view.button?.isHidden = true
        
        var config = SwiftMessages.Config()
        config.duration = .seconds(seconds: 3)
        
        if type == .success {
            view.configureTheme(.success)
        }
        else if type == .failure  {
            view.configureTheme(.error)
        }
        else {
            view.configureTheme(.info)
            config.duration = .seconds(seconds: 4)
        }
        
        view.configureContent(title: title, body: message)
        
        config.presentationStyle = .top
        config.presentationContext = .window(windowLevel: UIWindowLevelAlert)
        
        SwiftMessages.show(config: config, view: view)
    }
    
}

