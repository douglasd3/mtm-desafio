//
//  EmployeeDetailCoordinator.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 21/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import UIKit
import MessageUI

class EmployeeDetailCoordinator: Coordinator {
    
    var parentVC: UIViewController
    var viewModel: EmployeeDetailViewModel
    var viewController: UIViewController!
    
    init(parentVC: UIViewController, viewModel: EmployeeDetailViewModel) {
        self.parentVC = parentVC
        self.viewModel = viewModel
    }
    
    func start() {
        if let viewController = R.storyboard.main.employeeDetailVC() {            
            viewModel.coordinatorDelegate = self
            
            viewController.viewModel = viewModel
            
            self.viewController = viewController
            
            parentVC.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
}

extension EmployeeDetailCoordinator: EmployeeDetailVMCoordinatorDelegate {
    
    func didShowPhoneApp(withPhone phone: String) {
        UIApplication.shared.openURL(URL(string: "tel://\(phone)")!)
    }
    
    
    func didShowEmailComposer(withEmail email: String) {
        let mailComposeViewController = configuredMailComposeViewController(withEmail: email)
        if MFMailComposeViewController.canSendMail() {
            viewController.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            showSendMailErrorAlert()
        }
    }
    
}

// MARK: -  Helpers

extension EmployeeDetailCoordinator {
    
    func showSendMailErrorAlert() {
        MessagesUtil.showAlert(withTitle: R.string.localizable.failureMessageTitle(), message: R.string.localizable.employeeDetailEmailError(), andType: .failure)
    }
    
    func configuredMailComposeViewController(withEmail email: String) -> MFMailComposeViewController {
        if let viewController = viewController as? MFMailComposeViewControllerDelegate {
            let mailComposerVC = MFMailComposeViewController()
            mailComposerVC.mailComposeDelegate = viewController
            mailComposerVC.setToRecipients([email])
            mailComposerVC.setSubject(R.string.localizable.employeeDetailEmailTitle())
            mailComposerVC.setMessageBody("", isHTML: false)
            
            return mailComposerVC
        }
        
        return MFMailComposeViewController()
    }
    
}

