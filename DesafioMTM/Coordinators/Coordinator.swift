//
//  Coordinator.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 10/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Foundation

protocol Coordinator: class {
    
    func start()
    
}
