//
//  AppCoordinator.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 10/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import UIKit
import FirebaseAuth

class AppCoordinator: Coordinator {
    
    var window: UIWindow
    var currentUser = Auth.auth().currentUser
    
    var isLoggedIn: Bool {
        //try? Auth.auth().signOut()        
        return currentUser != nil
    }
    
    init(window: UIWindow) {        
        self.window = window
    }
    
    func start() {
        if isLoggedIn {
            showCathegories()
        } else {
            showAuthentication()
        }
    }
    
    func showAuthentication() {
        let authenticationCoordinator = AuthenticationCoordinator(window: window)
        authenticationCoordinator.start()
    }
    
    func showCathegories() {
        let categoriesCoordinator = CategoriesCoordinator(window: window)
        categoriesCoordinator.start()
    }
    
}

