//
//  CategoryDetailCoordinator.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 21/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import UIKit

class CategoryDetailCoordinator: Coordinator {
    
    var parentVC: UIViewController
    var viewModel: CategoryDetailViewModel
    var viewController: UIViewController!
    
    init(parentVC: UIViewController, viewModel: CategoryDetailViewModel) {
        self.parentVC = parentVC
        self.viewModel = viewModel
    }
    
    func start() {
        if let viewController = R.storyboard.main.categoryDetailVC() {        
            viewModel.viewModelDelegate = viewController
            viewModel.coordinatorDelegate = self
            
            viewController.viewModel = viewModel
            
            self.viewController = viewController
            
            parentVC.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
}

extension CategoryDetailCoordinator: CategoryDetailVMCoordinatorDelegate {
    
    func didSelectItem(viewModel: EmployeeDetailViewModel) {
        let employeeDetailCoordinator = EmployeeDetailCoordinator(parentVC: viewController, viewModel: viewModel)
        employeeDetailCoordinator.start()
    }
    
}
