//
//  AuthenticationCoordinator.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 10/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import UIKit

class AuthenticationCoordinator: Coordinator {
    
    let window: UIWindow
    var authenticationVC: AuthenticationVC?
    
    var newAccountCoordinator: NewAccountCoordinator?
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        if let viewController = R.storyboard.main.authenticationVC() {
            authenticationVC = viewController
            let viewModel = AuthenticationVM()
            viewModel.coordinatorDelegate = self
            viewModel.viewModelDelegate = viewController
            
            viewController.viewModel = viewModel
            window.rootViewController = UINavigationController(rootViewController: viewController)
        }        
    }
    
}

extension AuthenticationCoordinator: AuthenticationVMCoordinatorDelegate {
    
    func showNewAccountVC() {
        newAccountCoordinator = NewAccountCoordinator(window: window, parentVC: authenticationVC)
        newAccountCoordinator?.start()
    }
    
    func showCategoriesVC() {
        let categoriesCoordinator = CategoriesCoordinator(window: window)
        categoriesCoordinator.start()
    }
    
}
