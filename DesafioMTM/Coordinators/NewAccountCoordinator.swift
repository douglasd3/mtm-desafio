//
//  NewAccountCoordinator.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 12/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import UIKit

class NewAccountCoordinator: Coordinator {
    
    let window: UIWindow
    
    var parentVC: UIViewController?
    var viewController: UIViewController!
    
    init(window: UIWindow, parentVC: UIViewController?) {
        self.parentVC = parentVC
        self.window = window
    }
    
    func start() {
        if let viewController = R.storyboard.main.newAccountVC() {
            let viewModel = NewAccountVM()
            viewModel.viewModelDelegate = viewController
            viewModel.coordinatorDelegate = self
            
            viewController.viewModel = viewModel
            self.viewController = viewController
            if let parent = parentVC {
                viewController.setupModal(withTitle: R.string.localizable.newAccountTitle())
                parent.present(UINavigationController(rootViewController: viewController), animated: true, completion: nil)
            }
        }
    }
    
}

extension NewAccountCoordinator: NewAccountVMCoordinatorDelegate {
    
    func dismissNewAccountVC() {
        let categoriesCoordinator = CategoriesCoordinator(window: window)
        categoriesCoordinator.start()
    }
    
}
