//
//  CategoriesCoordinator.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 10/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import UIKit

class CategoriesCoordinator: Coordinator {
    
    let window: UIWindow
    var viewController: UIViewController!
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        if let viewController = R.storyboard.main.categoriesVC() {
            let viewModel = CategoriesVM()
            viewModel.viewModelDelegate = viewController
            viewModel.coordinatorDelegate = self
            
            viewController.viewModel = viewModel
            
            self.viewController = viewController
            
            window.rootViewController = UINavigationController(rootViewController: viewController)
        }
    }
    
}

extension CategoriesCoordinator: CategoriesVMCoordinatorDelegate {
    func didSelectItem(viewModel: CategoryDetailViewModel) {
        let categoryDetailCoordinator = CategoryDetailCoordinator(parentVC: viewController, viewModel: viewModel)
        categoryDetailCoordinator.start()
    }
    
}
