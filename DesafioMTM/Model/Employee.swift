//
//  Employee.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 21/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import ObjectMapper

struct Employee: Mappable {
    
    enum Key: String {
        case birthday, category, description, email, job, name, phone
    }
    
    var birthday: String!
    var category: String!
    var description: String!
    var email: String!
    var job: String!
    var name: String!
    var phone: String!
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        name          <- map[Key.name.rawValue]
        birthday          <- map[Key.birthday.rawValue]
        category          <- map[Key.category.rawValue]
        description          <- map[Key.description.rawValue]
        email          <- map[Key.email.rawValue]
        job          <- map[Key.job.rawValue]
        phone          <- map[Key.phone.rawValue]        
    }
    
}

