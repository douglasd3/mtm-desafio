//
//  Category.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 19/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import ObjectMapper

struct Category: Mappable {
    
    enum Key: String {
        case name, imagePath
    }

    var name: String!
    var imagePath: String!
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        name          <- map[Key.name.rawValue]
        imagePath     <- map[Key.imagePath.rawValue]
    }
    
}
