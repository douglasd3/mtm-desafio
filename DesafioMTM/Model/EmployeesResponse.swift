//
//  EmployeesResponse.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 21/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import ObjectMapper

struct EmployeesResponse: Mappable {
    
    enum Key: String {
        case employees
    }
    
    var employees: [Employee]!
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        employees <- map[Key.employees.rawValue]
    }
    
}

