//
//  CategoriesResponse.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 19/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import ObjectMapper

struct CategoriesResponse: Mappable {
    
    enum Key: String {
        case categories
    }
    
    var categories: [Category]!
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        categories <- map[Key.categories.rawValue]        
    }
    
}
