//
//  CategoryDetailProtocols.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 21/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Foundation

protocol CategoryDetailVMCoordinatorDelegate: Coordinator {
    
    func didSelectItem(viewModel: EmployeeDetailViewModel)
    
}

protocol CategoryDetailViewModel {
    
    var categoryName: String { get }
    var hasContent: Bool { get }
    var viewModelDelegate: CategoryDetailViewModelDelegate! { get set }
    var coordinatorDelegate: CategoryDetailVMCoordinatorDelegate? { get set}
    var dataSource: [Employee] { get set }
    
    func fetchEmployees()
    func itemSelected(item: EmployeeDetailViewModel)
    
}

protocol CategoryDetailViewModelDelegate: FirebaseApiDelegate {}
