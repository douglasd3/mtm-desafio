//
//  CategoryDetailVM.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 21/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Foundation

class CategoryDetailVM: CategoryDetailViewModel {
    
    // MARK: - Coordinator Delegate
    
    var coordinatorDelegate: CategoryDetailVMCoordinatorDelegate?
    
    // MARK: - ViewModel Delegate
    
    var viewModelDelegate: CategoryDetailViewModelDelegate!

    // MARK: - Firebase client
    
    var firebaseClient = FirebaseClient()
    
    // MARK: - Properties
    
    var dataSource: [Employee] = []
    
    var category: Category
    
    init(category: Category) {
        self.category = category
    }
    
}

// MARK: Computed Property

extension CategoryDetailVM {
    
    var categoryName: String {
        return category.name
    }
    
    var hasContent: Bool {
        return dataSource.count > 0
    }

}

// MARK: Helpers

extension CategoryDetailVM {
    
    func fetchEmployees() {
        firebaseClient.fetchEmployees(forCategoryName: categoryName) { (result) in
            switch result {
            case .Success(let employees):
                self.dataSource = employees
                print(self.dataSource)
                self.viewModelDelegate.apiCallDidFinish(error: nil, message: nil)
            case .Failure(let error):
                self.viewModelDelegate.apiCallDidFinish(error: error, message: R.string.localizable.categoryDetailApiError())
            }                        
        }
    }
    
    func itemSelected(item: EmployeeDetailViewModel) {
        coordinatorDelegate?.didSelectItem(viewModel: item)
    }
    
}


