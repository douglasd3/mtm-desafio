//
//  CategoryDetailItemVM.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 21/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Foundation

class CategoryDetailItemVM {
    
    let model: Employee
    
    var employeeName: String! {
        return model.name
    }
    
    var employeeJob: String! {
        return model.job
    }
    
    init(model: Employee) {
        self.model = model
    }
    
}

// MARK: Setup VategoryViewModel with category

extension CategoryDetailItemVM {
    
    func createDetailViewModel() -> EmployeeDetailViewModel {
        return EmployeeDetaillVM(employee: model)
    }
    
}
