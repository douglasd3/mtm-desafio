//
//  EmployeeDetailProtocols.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 21/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Foundation

protocol EmployeeDetailVMCoordinatorDelegate: Coordinator {
    
    func didShowPhoneApp(withPhone phone: String)
    func didShowEmailComposer(withEmail email: String)
    
}


protocol EmployeeDetailViewModel {
    
    var employeeName: String { get }
    
    var employeeBirthday: String { get }
    
    var employeeJob: String { get }
    
    var employeeEmail: String { get }
    
    var employeeDescription: String { get }
    
    var employeePhone: String { get }
    
    var employeeDetailTitle: String { get }
    
    var coordinatorDelegate: EmployeeDetailVMCoordinatorDelegate? { get set}
    
    func showPhoneApp()
    func showEmailComposer()
    
}

protocol EmployeeDetailViewModelDelegate: FirebaseApiDelegate {
    
    
}
