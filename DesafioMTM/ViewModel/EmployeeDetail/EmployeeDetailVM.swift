//
//  EmployeeDetailVM.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 21/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Foundation

class EmployeeDetaillVM: EmployeeDetailViewModel {

    
    // MARK: - Coordinator Delegate
    
    var coordinatorDelegate: EmployeeDetailVMCoordinatorDelegate?
    
    // MARK: - Properties
    
    var model: Employee
    
    
    init(employee: Employee) {
        model = employee
    }
    
}

// MARK: Computed Property

extension EmployeeDetaillVM {
    
    var employeeName: String {
        return model.name
    }
    
    var employeeBirthday: String {
        return model.birthday
    }
    
    var employeeJob: String {
        return model.job
    }
    
    var employeeEmail: String {
        return model.email
    }
    
    var employeeDescription: String {
        return model.description
    }
    
    var employeePhone: String {
        return model.phone
    }
    
    var employeeDetailTitle: String {
        return employeeName
    }

}

extension EmployeeDetailViewModel {
    
    func showPhoneApp() {
        coordinatorDelegate?.didShowPhoneApp(withPhone: employeePhone.onlyNumbers)
    }
    
    func showEmailComposer() {
        coordinatorDelegate?.didShowEmailComposer(withEmail: employeeEmail)
    }
    
}


