//
//  AuthenticationVM.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 10/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Foundation

class AuthenticationVM: AuthenticationViewModel {
    
    // MARK: - Coordinator Delegate
    
    var coordinatorDelegate: AuthenticationVMCoordinatorDelegate?
    
    // MARK: - ViewModel Delegate
    
    var viewModelDelegate: AuthenticationViewModelDelegate!
    
    // MARK: - Firebase client
    
    var firebaseClient = FirebaseClient()
    
    func showNewAccountVC() {
        coordinatorDelegate?.showNewAccountVC()
    }
    
}

// MARK: - Helpers

extension AuthenticationVM {
    
    func signIn(withEmail email: String, andPassword password: String) {
        viewModelDelegate.showIndicator()
        firebaseClient.signIn(withEmail: email, ansPassword: password) { result in
            switch result {
            case .Success(_):
                self.coordinatorDelegate?.showCategoriesVC()
            case .Failure(let error):
                if let error = error as NSError? {
                    let failureMessage = error.code == 17009 ? R.string.localizable.authenticationWrongUserError() : R.string.localizable.authenticationApiError()
                    self.viewModelDelegate.apiCallDidFinish(error: error, message: failureMessage)
                }
            }
            
            self.viewModelDelegate.dismissIndicator()
        }
    }
    
}
