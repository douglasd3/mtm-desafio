//
//  AuthenticationProtocols.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 10/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Foundation

protocol AuthenticationVMCoordinatorDelegate: Coordinator {
    
    func showNewAccountVC()
    func showCategoriesVC()
    
}

protocol AuthenticationViewModel {
    
    var viewModelDelegate: AuthenticationViewModelDelegate! { get set}
    var coordinatorDelegate: AuthenticationVMCoordinatorDelegate? { get set}
    
    func signIn(withEmail email: String, andPassword password: String)

}

protocol AuthenticationViewModelDelegate: FirebaseApiDelegate {
    
    func showIndicator()
    func dismissIndicator()
    
}
