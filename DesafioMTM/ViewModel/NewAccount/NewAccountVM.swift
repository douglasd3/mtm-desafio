//
//  NewAccountVM.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 12/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Foundation

class NewAccountVM: NewAccountViewModel {
    
    // MARK: - Coordinator Delegate
    
    var coordinatorDelegate: NewAccountVMCoordinatorDelegate?
    
    // MARK: - ViewModel Delegate
    
    var viewModelDelegate: NewAccountViewModelDelegate!
    
    // MARK: - Firebase client
    
    var firebaseClient = FirebaseClient()
    
}

// MARK: - Helpers

extension NewAccountVM {
    
    func sendNewAccount(withEmail email: String, _ password: String, _ phone: String, andCpf cpf: String) {
        viewModelDelegate.showIndicator()
        firebaseClient.signUp(withEmail: email, password, phone, andCpf: cpf) { result in
            switch result {
            case .Success(_):
                self.coordinatorDelegate?.dismissNewAccountVC()
            case .Failure(let error):
                self.viewModelDelegate.apiCallDidFinish(error: error, message: R.string.localizable.newAccountApiError())
            }
            
            self.viewModelDelegate.dismissIndicator()
        }
    }
    
}
