//
//  NewAccountProtocols.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 14/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Foundation

protocol NewAccountVMCoordinatorDelegate: Coordinator {
    
    func dismissNewAccountVC()
    
}

protocol NewAccountViewModel {
    
    var coordinatorDelegate: NewAccountVMCoordinatorDelegate? { get set}
    
    func sendNewAccount(withEmail email: String, _ password: String, _ phone: String, andCpf cpf: String)
    
}

protocol NewAccountViewModelDelegate: FirebaseApiDelegate {

    func showIndicator()
    func dismissIndicator()

}
