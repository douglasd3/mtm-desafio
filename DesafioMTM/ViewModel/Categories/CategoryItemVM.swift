//
//  CategoryItemVM.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 19/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Foundation
import Rswift

enum CategoryType: String {
    case android = "Android"
    case iOS = "iOS"
    case management = "Coordenação"
}

class CategoryItemVM {
    
    let model: Category
    
    var categoryName: String! {
        return model.name
    }
    
    var categoryImageResource: ImageResource? {
        switch categoryName {
        case CategoryType.android.rawValue:
            return R.image.ic_android
        case CategoryType.management.rawValue:
            return R.image.ic_management
        case CategoryType.iOS.rawValue:
            return R.image.ic_ios
        default:
            return nil
        }
    }
    
    init(model: Category) {
        self.model = model
    }
    
}

// MARK: Setup VategoryViewModel with category

extension CategoryItemVM {
    
    func createDetailViewModel() -> CategoryDetailViewModel {
        return CategoryDetailVM(category: model)
    }
    
}
