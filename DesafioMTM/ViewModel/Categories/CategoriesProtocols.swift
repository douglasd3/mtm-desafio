//
//  CategoriesProtocols.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 19/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Foundation

protocol CategoriesVMCoordinatorDelegate: Coordinator {
    
    func didSelectItem(viewModel: CategoryDetailViewModel)
    
}


protocol CategoriesViewModel {
    
    var coordinatorDelegate: CategoriesVMCoordinatorDelegate? { get set}
    var dataSource: [Category] { get set }
    var hasContent: Bool { get }
    
    func fetchCategories()
    func itemSelected(item: CategoryDetailViewModel)

    
}

protocol CategoriesViewModelDelegate: FirebaseApiDelegate {
    
    func showIndicator()
    func dismissIndicator()
    
}
