//
//  CategoriesVM.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 19/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Foundation

class CategoriesVM: CategoriesViewModel {
    
    // MARK: - Coordinator Delegate
    
    var coordinatorDelegate: CategoriesVMCoordinatorDelegate?
    
    // MARK: - ViewModel Delegate
    
    var viewModelDelegate: CategoriesViewModelDelegate!
    
    // MARK: - Firebase client
    
    var firebaseClient = FirebaseClient()
    
    var dataSource: [Category] = []
    
}

// MARK: Helpers

extension CategoriesVM {
    
    func fetchCategories() {
        viewModelDelegate.showIndicator()
        firebaseClient.fetchCategories { (result) in
            switch result {
            case .Success(let categories):
                print(categories)
                self.dataSource = categories
                self.viewModelDelegate.apiCallDidFinish(error: nil, message: nil)
            case .Failure(let error):
                self.viewModelDelegate.apiCallDidFinish(error: error, message: R.string.localizable.categoriesApiError())
            }
            
            self.viewModelDelegate.dismissIndicator()
        }
    }
    
    func itemSelected(item: CategoryDetailViewModel) {
        coordinatorDelegate?.didSelectItem(viewModel: item)
    }
        
}

// MARK: Computed Property

extension CategoriesVM {
    
    var hasContent: Bool {
        return dataSource.count > 0
    }
    
}

