//
//  FirebaseClient.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 19/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import Firebase
import FirebaseDatabase

protocol FirebaseApiDelegate {
    func apiCallDidFinish(error: Error?, message: String?)
}

struct FirebaseClient {
    
    var ref: DatabaseReference!
    
    enum APIResult<T> {
        case Success(T)
        case Failure(Error?)
    }
    
    init() {        
        ref = Database.database().reference()
    }
    
    func signUp(withEmail email: String, _ password: String, _ phone: String, andCpf cpf: String, completion: @escaping (APIResult<Bool>) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if error != nil {
                completion(.Failure(error))
            }
            else {
                if let user = Auth.auth().currentUser {
                    self.ref.child("users").child(user.uid).setValue(["cpf": cpf])
                    completion(.Success(true))
                }
                else {
                    completion(.Failure(error))
                }
            }
        }
    }
    
    func signIn(withEmail email: String, ansPassword password: String, completion: @escaping (APIResult<Bool>) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error != nil {
                completion(.Failure(error))
            }
            else {
                if Auth.auth().currentUser != nil {                    
                    completion(.Success(true))
                }
                else {
                    completion(.Failure(error))
                }
            }
        }
    }
    
    func fetchCategories(completion: @escaping (APIResult<[Category]>) -> Void) {
        ref.child("categoriesResponse").observeSingleEvent(of: .value, with: { snapshot in
            if let value = snapshot.value as? [String:Any] {
                if let categoriesResponse = CategoriesResponse(JSON: value) {
                    completion(.Success(categoriesResponse.categories))   
                }
            }
            
            
        }) { error in
            completion(.Failure(error))
        }        
    }
    
    func fetchEmployees(forCategoryName categoryName: String, completion: @escaping (APIResult<[Employee]>) -> Void) {
        // TODO: Filtrar por categoria diereto do firebase utilizando query
        ref.child("employeesResponse").observeSingleEvent(of: .value, with: { snapshot in
            if let value = snapshot.value as? [String:Any] {
                if let employeesResponse = EmployeesResponse(JSON: value) {
                    
                    let filteredEmployees = employeesResponse.employees.filter() { employee in
                        return employee.category == categoryName
                    }

                    completion(.Success(filteredEmployees))
                }
            }
            
            
        }) { error in
            completion(.Failure(error))
        }
    }

}

