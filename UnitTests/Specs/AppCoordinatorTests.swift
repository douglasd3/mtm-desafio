//
//  AppCoordinatorTests.swift
//  DesafioMTM
//
//  Created by Douglas Barbosa on 21/08/17.
//  Copyright © 2017 MTM. All rights reserved.
//

import XCTest
import Nimble
@testable import DesafioMTM

class AppCoordinatorTests: XCTestCase {
    
    var appCoordinator: AppCoordinator!
    var window = UIWindow()
    
    override func setUp() {
        super.setUp()
        
        appCoordinator = AppCoordinator(window: window)
    }
    
    override func tearDown() {
        appCoordinator = nil
        
        super.tearDown()
    }
    
    func testShouldShowLogin() {
        appCoordinator.currentUser = nil
        expect(self.appCoordinator.isLoggedIn).to(beFalse())
        
        appCoordinator.showAuthentication()
        
        let navigationController = appCoordinator.window.rootViewController
        expect(navigationController is UINavigationController) == true
        expect((navigationController as? UINavigationController)?.topViewController is AuthenticationVC) == true
        expect(self.window.rootViewController) == navigationController
    }
    
    func testShouldShowCategories() {
        //Firebase não permite criar uma instancia de User
        appCoordinator.currentUser = nil
        expect(!self.appCoordinator.isLoggedIn).to(beTrue())
        
        appCoordinator.showCathegories()
        
        let navigationController = appCoordinator.window.rootViewController
        expect(navigationController is UINavigationController) == true
        expect((navigationController as? UINavigationController)?.topViewController is CategoriesVC) == true
        expect(self.window.rootViewController) == navigationController
    }

    
}
