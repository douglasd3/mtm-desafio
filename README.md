# DesafioMTM #


 ObjectMapper - Parse de objetos JSON

 R.swift - Gerenciador de recursos como imagens, nibs, storyboard, string, etc

 SwiftyColor - Gerenciador de cores

 SpringIndicator - Indicador de atividade estilo android

 SnapKit - Criação de constraints via código

 StatefulViewController - Gerenciamento de estados de uma ViewController

 Eureka - Criação de formulários

 InputMask - Criação de mascaras em textFields, usado em conjunto com o Eureka

 PhoneNumberKit - Validação de números de telefone

 SwiftMessages - Alerts e Toasts estilizados
 
 Nimble - Asserts mais simplificados para teses unitarios

